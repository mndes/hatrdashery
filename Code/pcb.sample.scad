canvasX = 23;
canvasY = 19;
r = 180;
//r = 0;

rotate([0,r,0]){
    translate([0,0,6.2]){
        //STANDARD CANVAS BORDER
        canvasBorder(canvasX, canvasY);

        //STANDARD CANVAS
        difference(){
            cube([canvasX*2.8,canvasY*2.8,.8]);

            //THRU HOLES FOR TRACE WIRES
            for ( perfX = [10 : 12] ){
                perfY=8;
                perf(perfX, perfY);
            }

            perf(6, 8);
            perf(6, 9);
            perf(8, 8);
            perf(8, 9);
            perf(16, 8);
            perf(16, 9);
            perf(20, 9);

            for ( perfX = [11 : 13] ){
                perfY=9;
                perf(perfX, perfY);
            }
            perf(5, 9);

            for ( perfY = [13 : 19] ){
                perfX=15;
                perf(perfX, perfY);
            }

            //TRACE LINE ANCHORS
            //<-- pin 23 -> pin 3
            perfL(12, 6);
            stitch(down=22, over=6);
            stitch(down=22, over=15);
            perfL(17, 15);
            
            //<-- pin 21 -> pin 5
            perfL(11, 5);
            stitch(down=23, over=4);
            stitch(down=23, over=17);
            perfL(17, 17);

            //<-- pin 19 -> pin 6
            perfL(10, 6);
            stitch(down=10, over=2);
            stitch(down=24, over=2);
            stitch(down=24, over=18);
            perfL(17, 18);

            //--> pin 10 -> pin 7
            perfL(5, 11);
            stitch(down=5, over=19);
            perfL(13, 19);
            
            //--> pin 22 -> pin 4
            perfL(11, 11);
            stitch(down=11, over=16);
            perfL(13, 16);

            //--> pin 24 -> pin 2
            perfL(12, 11);
            perfL(12, 14);

            //--> pin 26 -> pin 1
            perfL(13, 11);
            perfL(13, 13);

            //THRU HOLES TO CONNECT COMPONENTS
            //inline "resistor"
            splice(down=24, over=8);
            splice(down=24, over=11);
            splice(down=23, over=8);

            //orientation
            index(1,7);
            index(15,12);
        }
    }

    //CONNECTING BLOCKS
    //keypad
    jumperU(15,13);
    jumperD(15,14);
    jumperU(15,15);
    jumperD(15,16);
    jumperU(15,17);
    jumperD(15,18);
    jumperU(15,19);

    //rpi
    jumperR(1,9);
    jumperR(5,9);
    jumperR(6,9);
    jumperR(8,9);
    jumperR(11,9);
    jumperR(12,9);
    jumperR(13,9);
    jumperR(16,9);
    jumperR(20,9);

    jumperL(1,8);
    jumperL(5,8);
    jumperL(6,8);
    jumperL(8,8);
    jumperL(10,8);
    jumperL(11,8);
    jumperL(12,8);
    jumperL(16,8);
    jumperL(20,8);
}


module canvasBorder(canvasX, canvasY){
    translate([0,0,-.7]){
        cube([1,canvasY*2.8,1.5]);
        cube([canvasX*2.8+1,1,1.5]);
        translate([(canvasX*2.8),0,0])
            cube([1,canvasY*2.8,1.5]);
        translate([0,canvasY*2.8,0])
            cube([canvasX*2.8+1,1,1.5]);
    }
}

//THRU HOLES
module perf(x, y){
    translate([((x-1)*2.54)+2.75,(y-1)*2.54+2.75,0])
        cube([1.2,1.2,12]);
}

module perfL(x, y){
    translate([((x-1)*2.54)+2.66,(y-1)*2.54+2.8,0])
        cube([1.4,1.4,2]);
}

module stitch(down, over){
    translate([((down-1)*2.54)+1.9,((over-1)*2.54)+2.8,0]){
        translate([0,1.8,0])
            cube([1.3,1.3,1.8]);
        translate([.6,-.8,0])
            cube([1.3,1.3,1.8]);
    }
}

module splice(down, over){
    translate([((down-1)*2.54)+1.9,((over-1)*2.54)+2.8,0]){
        translate([0,0,0])
            cube([1.9,1,1.8]);
        translate([0,1.8,0])
            cube([1.9,1,1.8]);
        //translate([.7,2.1,0])
        //    cube([1.1,1.1,1.8]);
    }
}

module index(x, y, r=90){
    translate([((x-1)*2.54)+3.25,(y-1)*2.54+2.75,0])
        rotate([0,0,r])
        cylinder(h=5, d=3, $fn=3);
}



//JUMPERS
module jumperL(x, y){
    color([0,1,0])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2.23,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.76,.62,-2])
                cube([1,1.2,14]);
            translate([1,-.3,-2])
                rotate([-10,0,0])
                    cube([.6,.6,6]);
        }
    }
}

module jumperR(x, y){
    color([0,0,1])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.75,.65,-2])
                cube([1,1.2,14]);
            translate([1,2.2,-2])
                rotate([10,0,0])
                    cube([.6,.6,6]);
        }
    }
}

module jumperU(x, y){
    color([.5,.5,0])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.75,.85,-2])
                cube([1.2,1,14]);
            translate([-.2,1.05,-2])
                rotate([0,10,0])
                    cube([.6,.6,6]);
        }
    }
}

module jumperD(x, y){
    color([.5,0,.5])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.55,.85,-2])
                cube([1.2,1,14]);
            translate([2.1,1.05,-2])
                rotate([0,-10,0])
                cube([.6,.6,6]);
        }
    }
}