rows=20;
cols=20;
//r = 0;
r = 180;

rotate([0,r,0]){
    translate([0,0,6.2]){
        //STANDARD CANVAS BORDER
        canvasBorder(rows, cols);

        //STANDARD CANVAS
        difference(){
            cube([rows*2.8,cols*2.8,.8]);

            //THRU HOLES FOR TRACE WIRES
            perf(4, 2);
            perf(20, 20);

            //TRACE LINE ANCHORS
            perfL(6, 2);
            stitch(down=20, over=1);
            perfL(20,18);

            //orientation
            index(1.75,2.25, r=0);
        }
    }
    //Jumpers
    jumperD(4,2);
    jumperL(20,20);
    jumperU(12,10);
    jumperR(10,12);
}

color([1,1,1]){
    rotate([r,r,0]){
        translate([2,2,-6])
        text("A B C D E F G H I J K L M N O P Q R S T", size=2);
    }
    rotate([r,r,0]){
        translate([-6,-4,-6]){
            translate([0,0*-2.54,0])
            text(" 1", size=2);
            translate([0,1*-2.54,0])
            text(" 2", size=2);
            translate([0,2*-2.54,0])
            text(" 3", size=2);
            translate([0,3*-2.54,0])
            text(" 4", size=2);
            translate([0,4*-2.54,0])
            text(" 5", size=2);
            translate([0,5*-2.54,0])
            text(" 6", size=2);
            translate([0,6*-2.54,0])
            text(" 7", size=2);
            translate([0,7*-2.54,0])
            text(" 8", size=2);
            translate([0,8*-2.54,0])
            text(" 9", size=2);
            translate([0,9*-2.54,0])
            text("10", size=2);
            translate([0,10*-2.54,0])
            text("11", size=2);
            translate([0,11*-2.54,0])
            text("12", size=2);
            translate([0,12*-2.54,0])
            text("13", size=2);
            translate([0,13*-2.54,0])
            text("14", size=2);
            translate([0,14*-2.54,0])
            text("15", size=2);
            translate([0,15*-2.54,0])
            text("16", size=2);
            translate([0,16*-2.54,0])
            text("17", size=2);
            translate([0,17*-2.54,0])
            text("18", size=2);
            translate([0,18*-2.54,0])
            text("19", size=2);
            translate([0,19*-2.54,0])
            text("20", size=2);
        }
    }
    
}




//OUTER EDGE OF BOARD FOR RIGIDITY
module canvasBorder(canvasX, canvasY){
    translate([0,0,-.7]){
        cube([1,canvasY*2.8,1.5]);
        cube([canvasX*2.8+1,1,1.5]);
        translate([(canvasX*2.8),0,0])
            cube([1,canvasY*2.8,1.5]);
        translate([0,canvasY*2.8,0])
            cube([canvasX*2.8+1,1,1.5]);
    }
}

//THRU HOLES
module perf(x, y){
    //Used to poke a hole in canvas for pins
    translate([((x-1)*2.54)+2.75,(y-1)*2.54+2.75,0])
        cube([1.2,1.2,12]);
}

module perfL(x, y){
    //Used to poke a hole in canvas for insulated
    //trace wire to transition from jumper
    translate([((x-1)*2.54)+2.66,(y-1)*2.54+2.8,0])
        cube([1.4,1.4,2]);
}

module stitch(down, over){
    //Used to poke 2 holes in canvas to anchor
    //trace wires
    translate([((down-1)*2.54)+1.9,((over-1)*2.54)+2.8,0]){
        translate([0,1.8,0])
            cube([1.3,1.3,1.8]);
        translate([.6,-.8,0])
            cube([1.3,1.3,1.8]);
    }
}

module splice(down, over){
    //Used to poke 2 large holes in canvas 
    //for twisting multiple components together
    translate([((down-1)*2.54)+1.9,((over-1)*2.54)+2.8,0]){
        translate([0,0,0])
            cube([1.9,1,1.8]);
        translate([0,1.8,0])
            cube([1.9,1,1.8]);
    }
}

module index(x, y, r=90){
    //Used to poke a triangular hole in canvas 
    //as an indicator
    translate([((x-1)*2.54)+3.25,(y-1)*2.54+2.75,0])
        rotate([0,0,r])
        cylinder(h=5, d=3, $fn=3);
}


//JUMPERS
module jumperL(x, y){
    color([0,1,0])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2.23,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.76,.62,-2])
                cube([1,1.2,14]);
            translate([1,-.3,-2])
                rotate([-10,0,0])
                    cube([.6,.6,6]);
        }
    }
}

module jumperR(x, y){
    color([0,0,1])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.75,.65,-2])
                cube([1,1.2,14]);
            translate([1,2.2,-2])
                rotate([10,0,0])
                    cube([.6,.6,6]);
        }
    }
}

module jumperU(x, y){
    color([.5,.5,0])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.75,.85,-2])
                cube([1.2,1,14]);
            translate([-.3,1.05,-2])
                rotate([0,10,0])
                    cube([.7,.6,6]);
        }
    }
}

module jumperD(x, y){
    color([.5,0,.5])
    translate([((x-1)*2.54)+2.1,((y-1)*2.54)+2,0]){
        difference(){
            cube([2.6,2.6,7]);
            translate([.65,.85,-2])
                cube([1.2,1,14]);
            translate([2.2,1.05,-2])
                rotate([0,-10,0])
                cube([.7,.6,6]);
        }
    }
}