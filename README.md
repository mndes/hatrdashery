## HATRdashery

![picture](images/readme1.jpeg)
![picture](images/readme.jpeg)

The wafers can be designed, printed and threaded with 24AWG wire to create circuit boards without the need for soldering. My original use for this module was to create custom Raspberry Pi HATs. 

---

## What's in the module:

1. Index: A triangular marker to indicate pin 1 or polarity for pins and connections.

2. Jumper: A cube with a specialized hole through the center that helps anchor and friction fit the stripped 24AWG wire and the pin header forming the connection block.

3. Perforation: A small hole in the wafer primarily for allow stripped 24AWG wires to be fed through and anchored to jumpers..

4. Large Perforation: A larger hole in the wafer to allow insulated 24AWG wire to be threaded thru and anchored to the wafer. These can also be used if you want to insert header pins on the topside of the wafer.

5. Stitch: A double perforation that allows the 24AWG wire to be threaded, routed and anchored to the wafer.

6. Splice: An elongated perforation that allows stripped 24AWG wires and/or bare components to be twisted/spliced together.

## Some basic hints:

- use a large perforation as the base of a jumper if you want a header on the top of the HAT
- Place large perforation 2 units away from the jumper for easier threading
- Alternate jumpers (U/D or R/L) on a single header to create greater isolation of traces
- I find it easiest to layout the wafer rotated to 0. Then before exporting to STL I rotate the wafer 180
- The "index" module has a rotate parameter, I use the rotate parameter in conjunction with tens of units to fine tune the placement of indexes

---

## Call to Action
As you can see, this bitbucket and module is pretty sparse. Let me know if you'd like to share your experiences and help build out this project.

If you could help with any of the following, please let me know:

- Improved Bitbucket documentation

- Create an end to end Instructable

- Port these modules to other CAD software (TinkerCAD, Native FreeCAD, SketchUp, etc)

- At some point, I think it would be great to use *Eagle* or *Circuits for TinkerCAD* to lay out the components, then have an STL produced using these basic objects.

## Contributions
- Dave Smith
